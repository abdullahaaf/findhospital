<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>FindHospital</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>
<div class="wrapper">
    <div class="sidebar" data-color="purple" data-image="assets/img/sidebar-5.jpg">
			<div class="sidebar-wrapper">
            <div class="logo">
                <a href="http://www.creative-tim.com" class="simple-text">
                    Find Hospital
                </a>
            </div>
            <ul class="nav">
                <li class="active">
                    <a href="maps.html">
                        <i class="pe-7s-map-marker"></i>
                        <p>Maps</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>
    <div class="main-panel">
		<nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Maps</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown"><p>Jenis Kecelakaan <b class="caret"></b></p></a>
													<ul class="dropdown-menu">
														<li><a href="mapsringan.php">Ringan</a></li>
														<li><a href="mapssedang.php">Sedang</a></li>
														<li><a href="mapsberat.php">Berat</a></li>
													</ul>
											</li>
											<li class="separator hidden-lg hidden-md"></li>
                    </ul>
                	</div>
            </div>
        </nav>
        <div id="map"></div>
    </div>
</div>


</body>

        <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

	<!--  Charts Plugin -->
	<script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC0A44ql_IBfKGXQMprfm-HnH5evFWLr-o sensor=false"></script> -->

		<!-- google maps api -->
		<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD75K04o6DqREG9HoTzQM5sUJ8x7f70yAk"></script> -->
		<script async defer
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD75K04o6DqREG9HoTzQM5sUJ8x7f70yAk&callback=initGoogleMaps"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="assets/js/light-bootstrap-dashboard.js"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="assets/js/demo.js"></script>

	<script type="text/javascript">

	var customLabel = {
			ringan: {
				label: 'R'
			},
			berat: {
				label: 'B'
			},
			sedang : {
				label : 'S'
			}
		};

		function initGoogleMaps() {

			var directionsDisplay = new google.maps.DirectionsRenderer();
			var directionsService = new google.maps.DirectionsService();

			var lat = -7.950858;
			var lng = 112.608149;

			var myLatlng = new google.maps.LatLng(lat,lng);
			var map = new google.maps.Map(document.getElementById("map"),{
				center : myLatlng,
				zoom : 10,
				styles: [{"featureType":"water","stylers":[{"saturation":43},{"lightness":-11},{"hue":"#0088ff"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"hue":"#ff0000"},{"saturation":-100},{"lightness":99}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#808080"},{"lightness":54}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#ece2d9"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#ccdca1"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#767676"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]},{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#b8cb93"}]},{"featureType":"poi.park","stylers":[{"visibility":"on"}]},{"featureType":"poi.sports_complex","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","stylers":[{"visibility":"simplified"}]}]
			});

			var infoWindow = new google.maps.InfoWindow;

			downloadUrl('phpsqlsearch_genxml.php', function(data){
				var xml = data.responseXML;
				var markers = xml.documentElement.getElementsByTagName('marker');
				Array.prototype.forEach.call(markers, function(markerElem){
					var nama_rumahsakit = markerElem.getAttribute('nama_rumahsakit');
						 var telepon = markerElem.getAttribute('telepon');
						 var alamat = markerElem.getAttribute('alamat');
						 var point = new google.maps.LatLng(
                  parseFloat(markerElem.getAttribute('lat')),
                  parseFloat(markerElem.getAttribute('lng')));

						var infowincontent = document.createElement('div');
						var strong = document.createElement('strong');
						strong.textContent = nama_rumahsakit
						infowincontent.appendChild(strong);
						infowincontent.appendChild(document.createElement('br'));

						var text = document.createElement('text');
						text.textContent = alamat
						infowincontent.appendChild(text);
						var icon = customLabel[type] || {};

						var marker = new google.maps.Marker({
							map : map,
							position : point
						});

						marker.addListener('click', function() {
											infoWindow.setContent(infowincontent);
											infoWindow.open(map, marker);
										});
				});
			});

			// downloadUrl('phpsqlsearch_genxml.php', function(data) {
      //       var xml = data.responseXML;
      //       var markers = xml.documentElement.getElementsByTagName('marker');
      //       Array.prototype.forEach.call(markers, function(markerElem) {
      //         var id = markerElem.getAttribute('id');
      //         var name = markerElem.getAttribute('name');
      //         var address = markerElem.getAttribute('address');
      //         var type = markerElem.getAttribute('type');
      //         var point = new google.maps.LatLng(
      //             parseFloat(markerElem.getAttribute('lat')),
      //             parseFloat(markerElem.getAttribute('lng')));
			//
      //         var infowincontent = document.createElement('div');
      //         var strong = document.createElement('strong');
      //         strong.textContent = name
      //         infowincontent.appendChild(strong);
      //         infowincontent.appendChild(document.createElement('br'));
			//
			// 				var text = document.createElement('text');
			// 				 text.textContent = address
			// 				 infowincontent.appendChild(text);
			// 				 var icon = customLabel[type] || {};
			// 				 var marker = new google.maps.Marker({
			// 					 map: map,
			// 					 position: point,
			// 					 label: icon.label
			// 				 });
			//
			// 				 marker.addListener('click', function(){
			// 					 infoWindow.setContent(infowincontent);
			// 					 infoWindow.open(map,marker);
			// 				 });

		// 	if (navigator.geolocation)
		// 	{
		// 		navigator.geolocation.getCurrentPosition(function(position)
		// 		{
		// 			var pos = {
		// 				lat : position.coords.latitude,
		// 				lng : position.coords.longitude
		// 			};
		// 			infoWindow.setPosition(pos);
		// 			infoWindow.setContent('Lokasi anda saat ini');
		// 			infoWindow.open(map);
		// 			map.setCenter(pos);
		// 		}, function()
		// 	{
		// 		handleLocationError(true, infoWindow, map.getCenter());
		// 	});
		// }else
		// {
		// 	handleLocationError(false, infoWindow, map.getCenter());
		// }

			// To add the marker to the map, call setMap();
			// marker.setMap(map);
		}

		function downloadUrl(url, callback) {
	var request = window.ActiveXObject ?
			new ActiveXObject('Microsoft.XMLHTTP') :
			new XMLHttpRequest;

	request.onreadystatechange = function() {
		if (request.readyState == 4) {
			request.onreadystatechange = doNothing;
			callback(request, request.status);
		}
	};

	request.open('GET', url, true);
	request.send(null);
}

function doNothing() {}

		// function handleLocationError(browserHasGeolocation, infoWindow, pos) {
		// 	infoWindow.setPosition(pos);
		// 	infoWindow.setContent(browserHasGeolocation ?
		// 	'Error : The geolocation service failed' :
		// 	'Error : Your browser doesn\'t support geolocation');
		//
		// 	infoWindow.open(map);
		// }


	</script>

    <!-- <script>
        $().ready(function(){
            demo.initGoogleMaps();
        });
    </script> -->

</html>
