<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Directions service</title>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #floating-panel {
        position: absolute;
        top: 10px;
        left: 25%;
        z-index: 5;
        background-color: #fff;
        padding: 5px;
        border: 1px solid #999;
        text-align: center;
        font-family: 'Roboto','sans-serif';
        line-height: 30px;
        padding-left: 10px;
      }
    </style>
  </head>
  <body>
    <div id="floating-panel">
    <b>Start: </b>
    <select id="start">
      <option value="surabaya, il">surabaya</option>
      <option value="malang, mo">malang</option>
      <option value="pasuruan, mo">pasuruan</option>
      <option value="bangil, ok">bangil</option>
      <option value="probolinggo, tx">probolinggo</option>
      <option value="gresik, nm">gresik</option>
      <option value="tulungagung, az">tulungagung</option>
      <option value="kediri, az">kediri</option>
      <option value="nganjuk, az">nganjuk</option>
      <option value="lawang, ca">lawang</option>
      <option value="mojokerto, ca">mojokerto</option>
      <option value="blitar, ca">blitar</option>
    </select>
    <b>End: </b>
    <select id="end">
       <option value="surabaya, il">surabaya</option>
      <option value="malang, mo">malang</option>
      <option value="pasuruan, mo">pasuruan</option>
      <option value="bangil, ok">bangil</option>
      <option value="probolinggo, tx">probolinggo</option>
      <option value="gresik, nm">gresik</option>
      <option value="tulungagung, az">tulungagung</option>
      <option value="kediri, az">kediri</option>
      <option value="nganjuk, az">nganjuk</option>
      <option value="lawang, ca">lawang</option>
      <option value="mojokerto, ca">mojokerto</option>
      <option value="blitar, ca">blitar</option>
    </select>
    </div>
    <div id="map"></div>
    <script>
      function initMap() {
        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 7,
          center: {lat: -7.984528, lng: 112.620253}
        });
        directionsDisplay.setMap(map);

        var onChangeHandler = function() {
          calculateAndDisplayRoute(directionsService, directionsDisplay);
        };
        document.getElementById('start').addEventListener('change', onChangeHandler);
        document.getElementById('end').addEventListener('change', onChangeHandler);
      }

      function calculateAndDisplayRoute(directionsService, directionsDisplay) {
        directionsService.route({
          origin: document.getElementById('start').value,
          destination: document.getElementById('end').value,
          travelMode: 'DRIVING'
        }, function(response, status) {
          if (status === 'OK') {
            directionsDisplay.setDirections(response);
          } else {
            window.alert('Directions request failed due to ' + status);
          }
        });
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCuCdn2H7xLs7sHLV2KPOsS3Xc8ksDn1gU&callback=initMap">
    </script>
  </body>
</html>
