<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>FindHospital</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body onload="initGoogleMaps()">
<div class="wrapper">
    <div class="sidebar" data-color="purple" data-image="assets/img/sidebar-5.jpg">
			<div class="sidebar-wrapper">
            <div class="logo">
                <a href="http://www.creative-tim.com" class="simple-text">
                    Find Hospital
                </a>
            </div>
            <ul class="nav">
                <li class="active">
                    <a href="maps.html">
                        <i class="pe-7s-map-marker"></i>
                        <p>Maps</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>
    <div class="main-panel">
		<nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Maps</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown"><p>Jenis Kecelakaan <b class="caret"></b></p></a>
													<ul class="dropdown-menu">
														<li><a href="mapsringan.php">Ringan</a></li>
														<li><a href="mapssedang.php">Sedang</a></li>
														<li><a href="mapsberat.php">Berat</a></li>
													</ul>
											</li>
											<li class="separator hidden-lg hidden-md"></li>
                    </ul>

                    <?php
                    $con = mysql_connect("localhost","root","");
                    $db = mysql_select_db("findhospital",$con);
										$lat = -7.950858;
										$lng = 112.608149;

										// $query = "SELECT * FROM Rumahsakit_data WHERE 1";
										$query = "SELECT nama_rumahsakit, telepon, alamat, lat,lng ,111.045 * DEGREES(ACOS(COS(RADIANS(".$lat.")) * COS(RADIANS(lat)) * COS(RADIANS(lng) - RADIANS(".$lng.")) + SIN(RADIANS(".$lat.")) * SIN(RADIANS(lat)))) AS distance_in_km FROM Rumahsakit_data ORDER BY distance_in_km ASC LIMIT 0,5";
										$get = mysql_query($query);
										$gett = mysql_query($query);
										?>

                    <ul class="nav navbar-nav navbar-right">
                        <!-- <select class="form-control" name="" id="end">
                          <?php
                          while ($row=mysql_fetch_assoc($get))
                          {
                            ?>
                            <option value="<?php echo ($row['lat,lng'])?>"><?php echo ($row['nama_rumahsakit'])?></option>
                            <?php
                          }
                          ?>
                        </select> -->
                    </ul>
										<!-- <ul class="nav navbar-nav navbar-right">
												<select class="form-control" name="" id="start">
													<?php
													while ($roww=mysql_fetch_assoc($gett))
													{
														?>
														<option value="<?php echo ($roww['lat,lng'])?>"><?php echo ($roww['nama_rumahsakit'])?></option>
														<?php
													}
													?>
												</select>
										</ul> -->
                	</div>
            </div>
        </nav>
        <div id="map" style="width : 100%; height : 300px;"></div>
				<div class="content">
					<form class="form-horizontal" action="#" onsubmit="calcRoute(); return false;" id="routeForm">
						<div class="form-group">
							<label for="routeStart" class="col-sm-2 control-label">Dari:</label>
							<div class="col-md-6">
								<input type="text" class="form-control" id="routeStart" name="routeStart" placeholder="Dimana Anda..." value="">
							</div>
						</div><br>
						<div class="form-group">
							<label for="routeEnd" class="col-sm-2 control-label">Tujuan Akhir:</label>
							<div class="col-md-6">
								<input type="text" class="form-control"  id="routeEnd" name="routeEnd" placeholder="Tujuan Akhir anda..." value="">
							</div>
						</div><br>
						<p>pilih mode berkendara : </p>
							<div class="radio">
								<label><input type="radio" name="travelMode" value="DRIVING" checked>Berkendara</label>
							</div>
							<!-- <div class="radio">
								<label><input type="radio" name="travelMode" value="BICYCLING">Bersepeda</label>
							</div>
							<div class="radio">
								<label><input type="radio" name="travelMode" value="TRANSIT">Transportasi Umum</label>
							</div> -->
							<div class="radio">
								<label><input type="radio" name="travelMode" value="WALKING">Jalan Kaki</label>
							</div><br>
							  <input type="submit" class="btn btn-primary" value="Temukan Rute">
					</form><br>
					<div id="directionsPanel">
					  Masukkan tempat asal anda dan klik "Temukan Rute".
					</div>
				</div>
    </div>
</div>


</body>

        <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

	<!--  Charts Plugin -->
	<script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC0A44ql_IBfKGXQMprfm-HnH5evFWLr-o sensor=false"></script> -->

		<!-- google maps api -->
		<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD75K04o6DqREG9HoTzQM5sUJ8x7f70yAk"></script> -->
		<!-- <script async defer
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD75K04o6DqREG9HoTzQM5sUJ8x7f70yAk&callback=initGoogleMaps"></script> -->
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyD75K04o6DqREG9HoTzQM5sUJ8x7f70yAk&sensor=false"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="assets/js/light-bootstrap-dashboard.js"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="assets/js/demo.js"></script>

	<script type="text/javascript">

	var directionsDisplay, mapRoute;
	var directionsService = new google.maps.DirectionsService();
	var geocoder = new google.maps.Geocoder();

	var customLabel = {
			ringan: {
				label: 'R'
			},
			berat: {
				label: 'B'
			},
			sedang : {
				label : 'S'
			}
		};

		function initGoogleMaps() {

			var lat = -7.950858;
			var lng = 112.608149;

			var rendererOptions = {draggable : true};
			directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);
			
			var myLatlng = new google.maps.LatLng(lat,lng);
			var map = new google.maps.Map(document.getElementById("map"),{
				center : myLatlng,
				zoom : 13,
				mapTypeId : google.maps.MapTypeId.ROADMAP,
				mapTypeControl : false,
				styles: [{"featureType":"water","stylers":[{"saturation":43},{"lightness":-11},{"hue":"#0088ff"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"hue":"#ff0000"},{"saturation":-100},{"lightness":99}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#808080"},{"lightness":54}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#ece2d9"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#ccdca1"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#767676"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]},{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#b8cb93"}]},{"featureType":"poi.park","stylers":[{"visibility":"on"}]},{"featureType":"poi.sports_complex","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","stylers":[{"visibility":"simplified"}]}]
			});

			directionsDisplay.setMap(map);
			directionsDisplay.setPanel(document.getElementById("directionsPanel"));

			var infoWindow = new google.maps.InfoWindow;

			downloadUrl('phpsqlringan.php', function(data){
				var xml = data.responseXML;
				var markers = xml.documentElement.getElementsByTagName('marker');
				Array.prototype.forEach.call(markers, function(markerElem){
					var nama_rumahsakit = markerElem.getAttribute('nama_rumahsakit');
						 var telepon = markerElem.getAttribute('telepon');
						 var alamat = markerElem.getAttribute('alamat');
						 var point = new google.maps.LatLng(
                  parseFloat(markerElem.getAttribute('lat')),
                  parseFloat(markerElem.getAttribute('lng')));

						var infowincontent = document.createElement('div');
						var strong = document.createElement('strong');
						strong.textContent = nama_rumahsakit
						infowincontent.appendChild(strong);
						infowincontent.appendChild(document.createElement('br'));

						var text = document.createElement('text');
						text.textContent = alamat
						infowincontent.appendChild(text);
						var icon = customLabel[type] || {};

						var marker = new google.maps.Marker({
							map : map,
							position : point
						});

						marker.addListener('click', function() {
											infoWindow.setContent(infowincontent);
											infoWindow.open(map, marker);
										});
				});
			});

			// if (navigator.geolocation) {
			// 	navigator.geolocation.getCurrentPosition(foundYou,notFound);
			// }else {
			// 	{
			// 		alert('Geolocation not supported or not enabled');
			// 	}
			// }
		}

		// function notFound(msg)
		// {
		// 	alert('Could not find your location')
		// }
		// function foundYou(position){
		// 				var myLatlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
		//
		// 				geocoder.geocode({'latlng':latlng}, function(results,status){
		// 					if (status == google.maps.GeocoderStatus.OK) {
		// 						if (results[0]){
		// 							marker = new google.maps.Marker({
		// 								position : myLatlng,
		// 								map : map
		// 							});
		//
		// 							var address = results[0].address_components[1].long_name+' '+results[0].address_components[0].long_name+', '+results[0].address_components[3].long_name
		// 							// set the located address to the link, show the link and add a click event handler
		// 							$('.autoLink span').html(address).parent().show().click(function(){
		// 								// onclick, set the geocoded address to the start-point formfield
		// 								$('#routeStart').val(address);
		// 								// call the calcRoute function to start calculating the route
		// 								calcRoute();
		// 							});
		// 						}
		// 					}
		// 					else {
		// 						alert("Geocoder failed due to:" + status);
		// 					}
		// 				});
		// 			}

					function calcRoute() {
					  // get the travelmode, startpoint and via point from the form
					  var travelMode = $('input[name="travelMode"]:checked').val();
					  var start = $("#routeStart").val();
					  var end = $("#routeEnd").val();
					  // compose a array with options for the directions/route request
					  var request = {
					    origin: start,
					    destination: end,
					    unitSystem: google.maps.UnitSystem.IMPERIAL,
					    travelMode: google.maps.DirectionsTravelMode[travelMode]
					  };
					  // call the directions API
					  directionsService.route(request, function(response, status) {
					    if (status == google.maps.DirectionsStatus.OK) {
					      // directions returned by the API, clear the directions panel before adding new directions
					      $('#directionsPanel').empty();
					      // display the direction details in the container
					      directionsDisplay.setDirections(response);
					    } else {
					      // alert an error message when the route could nog be calculated.
					      if (status == 'ZERO_RESULTS') {
					        alert('No route could be found between the origin and destination.');
					      } else if (status == 'UNKNOWN_ERROR') {
					        alert('A directions request could not be processed due to a server error. The request may succeed if you try again.');
					      } else if (status == 'REQUEST_DENIED') {
					        alert('This webpage is not allowed to use the directions service.');
					      } else if (status == 'OVER_QUERY_LIMIT') {
					        alert('The webpage has gone over the requests limit in too short a period of time.');
					      } else if (status == 'NOT_FOUND') {
					        alert('At least one of the origin, destination, or waypoints could not be geocoded.');
					      } else if (status == 'INVALID_REQUEST') {
					        alert('The DirectionsRequest provided was invalid.');
					      } else {
					        alert("There was an unknown error in your request. Requeststatus: nn"+status);
					      }
					    }
					  });
					}

		function downloadUrl(url, callback) {
	var request = window.ActiveXObject ?
			new ActiveXObject('Microsoft.XMLHTTP') :
			new XMLHttpRequest;

	request.onreadystatechange = function() {
		if (request.readyState == 4) {
			request.onreadystatechange = doNothing;
			callback(request, request.status);
		}
	};

	request.open('GET', url, true);
	request.send(null);
}

function doNothing() {}

		function handleLocationError(browserHasGeolocation, infoWindow, pos) {
			infoWindow.setPosition(pos);
			infoWindow.setContent(browserHasGeolocation ?
			'Error : The geolocation service failed' :
			'Error : Your browser doesn\'t support geolocation');

			infoWindow.open(map);
		}


	</script>

</html>
