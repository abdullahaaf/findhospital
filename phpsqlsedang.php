<?php
require 'koneksi.php';

function parseToXML($htmlStr)
{
$xmlStr=str_replace('<','&lt;',$htmlStr);
$xmlStr=str_replace('>','&gt;',$xmlStr);
$xmlStr=str_replace('"','&quot;',$xmlStr);
$xmlStr=str_replace("'",'&#39;',$xmlStr);
$xmlStr=str_replace("&",'&amp;',$xmlStr);
return $xmlStr;
}

$connection=mysql_connect ('localhost', $username, $password);
if (!$connection) {
die('Not connected : ' . mysql_error());
}

$db_selected = mysql_select_db($database, $connection);
if (!$db_selected) {
die ('Can\'t use db : ' . mysql_error());
}

$lat = -7.950858;
$lng = 112.608149;

// $query = "SELECT * FROM Rumahsakit_data WHERE 1";
$query = "SELECT nama_rumahsakit, telepon, alamat, lat,lng ,111.045 * DEGREES(ACOS(COS(RADIANS(".$lat.")) * COS(RADIANS(lat)) * COS(RADIANS(lng) - RADIANS(".$lng.")) + SIN(RADIANS(".$lat.")) * SIN(RADIANS(lat)))) AS distance_in_km FROM Rumahsakit_data WHERE Rumahsakit_data.kategori_kecelakaan='sedang' OR Rumahsakit_data.kategori_kecelakaan='berat' ORDER BY distance_in_km ASC LIMIT 0,5";
$result = mysql_query($query);

if (!$result) {
die('Invalid query: ' . mysql_error());
}

header("Content-type: text/xml");
echo '<markers>';

while($row = @mysql_fetch_assoc($result))
{
  echo '<marker ';
  echo 'nama_rumahsakit="' . parseToXML($row['nama_rumahsakit']) . '" ';
  echo 'telepon="' . $row['telepon'] . '" ';
  echo 'alamat="' . $row['alamat'] . '" ';
  echo 'lat="' . $row['lat'] . '" ';
  echo 'lng="' . $row['lng'] . '" ';
  echo '/>';
}

echo '</markers>';
?>
